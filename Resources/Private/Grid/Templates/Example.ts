tx_gridelements.setup.example {
  title = Beispiel
  #icon = EXT:example_extension/Resources/Public/Images/BackendLayouts/default.gif
  config {
    colCount = 2
    rowCount = 1
    rows {
      1 {
        columns {
          1 {
            name = Links
            colPos = 0
            allowed = *
          }
          2 {
            name = Rechts
            colPos = 1
            allowed = *
          }
        }
      }
    }
  }
}