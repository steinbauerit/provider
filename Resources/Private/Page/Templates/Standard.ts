
mod.web_layout.BackendLayouts {
  Standard {
    title = Standard
    icon = EXT:base/ext_icon.png
    config {
      backend_layout {
        colCount = 1
        rowCount = 1
        rows {
          1 {
            columns {
              1 {
                name = Inhalt
                colPos = 0
              }
            }
          }
        }
      }
    }
  }
}