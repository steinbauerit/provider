plugin.tx_vhs.settings.asset {
    jquery {
    	path = https://code.jquery.com/jquery-3.2.1.min.js
    	external = 1
    }
    lightcaseJs {
    	path = https://cdnjs.cloudflare.com/ajax/libs/lightcase/2.4.0/js/lightcase.min.js
    	external = 1
    	dependencies = jquery
    }
    cookieconsentJs {
        path = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js
        external = 1
    }
    baseJs {
        path = EXT:base/Resources/Public/Scripts/base.js
    	dependencies = jquery,lightcaseJs,cookieconsentJs
    }

    lightcaseCss {
    	path = https://cdnjs.cloudflare.com/ajax/libs/lightcase/2.4.0/css/lightcase.min.css
    	external = 1
    }
    cookieconsentCss {
        path = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css
        external = 1
    }
    baseCss {
        path = EXT:base/Resources/Public/Css/base.css
    	dependencies = lightcaseCss,cookieconsentCss
    }
}
