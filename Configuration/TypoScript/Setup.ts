page.10 {
      templateRootPaths {
         20 = EXT:provider/Resources/Private/Page/Templates
      }
      partialRootPaths {
         20 = EXT:provider/Resources/Private/Page/Partials
      }
      layoutRootPaths {
         20 = EXT:provider/Resources/Private/Page/Layouts
      }
   }
}

lib.gridelements.baseSetup.cObject {
   templateRootPaths {
      20 = EXT:provider/Resources/Private/Grid/Templates
   }
   partialRootPaths {
      20 = EXT:provider/Resources/Private/Grid/Partials
   }
   layoutRootPaths {
      20 = EXT:provider/Resources/Private/Grid/Layouts
   }
}
